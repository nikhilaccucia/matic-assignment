import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class UserCircle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 20.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          CircleAvatar(
            backgroundImage: AssetImage('assets/user_avatar.png'),
            radius: 26,
          ),
          SizedBox(
            height: 4,
          ),
          Text(
            'Dalpat',
            style: GoogleFonts.sourceSansPro(
                fontSize: 13,
                letterSpacing: 1.4,
                color: Colors.white,
                fontWeight: FontWeight.w100),
          )
        ],
      ),
    );
  }
}
