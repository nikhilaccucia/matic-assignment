import 'package:flutter/material.dart';
import 'package:matic_assignment/utils/constants.dart';

class ProfileUserData extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        'Arthur Cooper',
        style: TextStyle(
            color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
      ),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 4.0),
            child: Text('arthurcooper@live.com', style: Constants.textTheme14),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 4.0),
            child: Text('(629) 555-0129', style: Constants.textTheme14),
          ),
        ],
      ),
      trailing: Image.asset('assets/user_avatar.png'),
    );
  }
}
