import 'package:flutter/material.dart';
import 'package:matic_assignment/utils/constants.dart';

class TotalBalance extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 24.0, left: 10),
          child: Text(
            '\$20.2',
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 34),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 30.0, left: 10),
          child: Text(
            'USDC Balance',
            style: Constants.textTheme14,
          ),
        ),
      ],
    );
  }
}
