import 'package:flutter/material.dart';
import 'package:matic_assignment/ui/widgets/profile_user_data.dart';
import 'package:matic_assignment/ui/widgets/total_balance_widget.dart';
import 'package:matic_assignment/ui/widgets/transaction_buttons.dart';

class ProfilePageHeader extends StatefulWidget {
  @override
  _ProfilePageHeaderState createState() => _ProfilePageHeaderState();
}

class _ProfilePageHeaderState extends State<ProfilePageHeader> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Image.asset(
          'assets/profile_design.png',
          fit: BoxFit.fill,
          height: 310,
          color: Color.fromRGBO(57, 70, 162, 1),
          width: MediaQuery.of(context).size.width,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20.0, left: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              IconButton(
                  icon: Icon(
                    Icons.arrow_back,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
              ProfileUserData(),
              TotalBalance(),
              TransactionButtons()
            ],
          ),
        )
      ],
    );
  }
}
