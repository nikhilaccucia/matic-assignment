import 'package:flutter/material.dart';
import 'package:matic_assignment/ui/pages/profile_page.dart';
import 'package:page_transition/page_transition.dart';

class HeaderButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 20.0),
          child: Transform(
            transform: Matrix4.identity()..scale(1.3),
            child: InputChip(
              avatar: CircleAvatar(
                backgroundImage: AssetImage('assets/user_avatar.png'),
                radius: 30,
              ),
              label: Text(
                '\$123.39',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 14,
                ),
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    PageTransition(
                        type: PageTransitionType.downToUp,
                        child: ProfilePage()));
              },
            ),
          ),
        ),
      ],
    );
  }
}
