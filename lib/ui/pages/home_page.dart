import 'dart:ui';

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:matic_assignment/data/planets_model.dart';
import 'package:matic_assignment/repository/api.dart';
import 'package:matic_assignment/ui/widgets/bottom_splitted_button.dart';
import 'package:matic_assignment/ui/widgets/header_button.dart';
import 'package:matic_assignment/ui/widgets/user_circle.dart';
import 'package:simple_animations/simple_animations.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Future<List<PlanetsModel>> planetsData;

  @override
  void initState() {
    super.initState();
    // Getting the data at once to not make too many request on network
    planetsData = Api.getAllItems();
  }

  // animating background by changing HUE of the color
  static Color changeColorHue(double hue) =>
      HSLColor.fromColor(Color.fromRGBO(64, 79, 183, 1)).withHue(hue).toColor();
  final tween = MultiTrackTween([
    Track("color1").add(Duration(seconds: 4),
        ColorTween(begin: changeColorHue(235), end: changeColorHue(240))),
    Track("color2").add(Duration(seconds: 4),
        ColorTween(begin: changeColorHue(235), end: changeColorHue(240))),
  ]);

  @override
  Widget build(BuildContext context) {
    // for responsive fonts on all screens
    ScreenUtil.init(context);

//If the design is based on the size of the iPhone6 ​​(iPhone6 ​​750*1334)
    ScreenUtil.init(context, width: 414, height: 896);

//If you want to set the font size is scaled according to the system's "font size" assist option
    ScreenUtil.init(context, width: 414, height: 896, allowFontScaling: true);
    return SafeArea(
      top: window.viewPadding.top > 0 ? true : false,
      bottom: window.viewPadding.bottom > 0 ? true : false,
      child: Scaffold(
        backgroundColor: Color.fromRGBO(64, 79, 183, 1),
        body: ControlledAnimation(
            playback: Playback.PLAY_FORWARD,
            tween: tween,
            duration: tween.duration,
            builder: (context, animation) {
              return Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                      animation["color1"],
                      animation["color2"],
                      // animation["color3"]
                    ])),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    headerColumn(),
                    bodyColumn(),
                  ],
                ),
              );
            }),
      ),
    );
  }

  Widget headerColumn() {
    return Column(
      children: [
        SizedBox(
          height: 20,
        ),
        // chip navigation button
        HeaderButton(),
      ],
    );
  }

  bodyColumn() {
    return Column(
      children: [
        getAnimatedText(),
        SizedBox(
          height: 10,
        ),
        // User MugShot
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: List.generate(
              4,
              (index) => UserCircle(),
            )),
        // Request and Pay button
        BottomSplittedButton(),
      ],
    );
  }

  getAnimatedText() {
    return FutureBuilder(
        future: planetsData,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            // reducing the description length to fit the desing from Figma
            var minifiedFirst = getMinifiedString(snapshot.data[0].explanation);

            var minifiedSecond =
                getMinifiedString(snapshot.data[1].explanation);

            var minifiedThird = getMinifiedString(snapshot.data[2].explanation);
            return Column(
              children: [
                Padding(
                  padding:
                      const EdgeInsets.only(left: 24.0, right: 24, bottom: 10),
                  child: FadeAnimatedTextKit(
                    isRepeatingAnimation: true,
                    repeatForever: true,
                    duration: Duration(seconds: 2),
                    onTap: () {
                      print("Tap Event");
                    },
                    text: [
                      snapshot.data[0].title,
                      snapshot.data[1].title,
                      snapshot.data[2].title,
                    ],
                    textStyle: GoogleFonts.prata(
                        fontSize: ScreenUtil().setSp(32),
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 24.0, right: 24, bottom: 10),
                  child: FadeAnimatedTextKit(
                    repeatForever: true,
                    duration: Duration(seconds: 2),
                    onTap: () {
                      print("Tap Event");
                    },
                    text: [
                      minifiedFirst,
                      minifiedSecond,
                      minifiedThird,
                    ],
                    textStyle: GoogleFonts.sourceSansPro(
                        fontSize: ScreenUtil().setSp(16),
                        letterSpacing: 1.4,
                        color: Colors.white,
                        fontWeight: FontWeight.w100),
                  ),
                ),
              ],
            );
          } else {
            return CircularProgressIndicator();
          }
        });
  }

  String getMinifiedString(String text) {
    return text.replaceRange(200, text.length, '.');
  }
}
