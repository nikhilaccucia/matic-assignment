import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:matic_assignment/ui/widgets/profile_page_header.dart';
import 'package:matic_assignment/utils/constants.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: window.viewPadding.top > 0 ? true : false,
      bottom: window.viewPadding.bottom > 0 ? true : false,
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //ProfileInformationWidget
              profileHeader(), // Profile Header
              nonUsdBalance(), // Non USD Balance container
              divider(),
              userSpecificData(), // Rewards and transaction list
              divider(),
              logoutButton(), // Logout Button
              divider(),
              appVersion(), // App Version Code
            ],
          ),
        ),
      ),
    );
  }

  Widget profileHeader() {
    return Container(
        color: Color.fromRGBO(70, 87, 202, 1),
        height: 310,
        width: MediaQuery.of(context).size.width,
        child: ProfilePageHeader());
  }

  Widget userSpecificData() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: ListTile(
            leading: SvgPicture.asset('assets/Selected.svg'),
            title: Text(
              'My Rewards',
              style: Constants.textTheme16Bold,
            ),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  '120 Points',
                  style: Constants.textTheme16,
                ),
                SizedBox(
                  width: 20,
                ),
                SvgPicture.asset('assets/right_arrow.svg')
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: ListTile(
              leading: SvgPicture.asset('assets/Selected.svg'),
              title: Text('All Transactions', style: Constants.textTheme16Bold),
              trailing: SvgPicture.asset('assets/right_arrow.svg')),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: ListTile(
              leading: SvgPicture.asset('assets/Selected.svg'),
              title: Text(
                'Feedback',
                style: Constants.textTheme16Bold,
              ),
              trailing: SvgPicture.asset('assets/right_arrow.svg')),
        ),
      ],
    );
  }

  nonUsdBalance() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListTile(
            title: Text(
              'Non USD Balance',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
            ),
            subtitle: Text(
              'Balance from other tokens',
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 11),
            ),
            trailing: Text('\$243.44', style: Constants.textTheme16),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 24.0, top: 10),
          child: Text(
            'Convert to USD',
            style: TextStyle(
                color: Color.fromRGBO(71, 88, 203, 1),
                fontSize: 14,
                fontWeight: FontWeight.normal),
          ),
        ),
      ],
    );
  }

  logoutButton() {
    return Padding(
      padding: const EdgeInsets.only(left: 24.0, top: 10),
      child: Text(
        'Logout',
        style: TextStyle(
            color: Colors.red, fontSize: 16, fontWeight: FontWeight.bold),
      ),
    );
  }

  appVersion() {
    return Padding(
      padding: const EdgeInsets.only(left: 24.0, top: 10),
      child: Text(
        'Version 1.0',
        style: TextStyle(color: Colors.grey, fontSize: 10),
      ),
    );
  }

  divider() {
    return Padding(
      padding:
          const EdgeInsets.only(left: 25.0, right: 25, top: 15, bottom: 10),
      child: Divider(),
    );
  }
}
