import 'dart:convert';
import 'package:matic_assignment/data/planets_model.dart';
import 'package:http/http.dart' as http;

class Api {
  // for Single Data without date , just for refrence not using this api call
  static Future<PlanetsModel> fetchPlanetsData() async {
    final response = await http.get(
        'https://api.nasa.gov/planetary/apod?api_key=gxaT6SMN2cFEHJDiv6hs8KJcp9QXr8kTYiuseS5u');

    if (response.statusCode == 200) {
      return PlanetsModel.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load data');
    }
  }

  // Data for Multiple Date using Future Wait
  static Future<List<PlanetsModel>> getAllItems() async {
    var client = http.Client();
    List<String> dates = [
      '2020-06-02',
      '2020-07-04',
      '2020-07-07'
    ]; //different dates

    List<http.Response> list = await Future.wait(dates.map((date) => client.get(
        'https://api.nasa.gov/planetary/apod?api_key=z7zkM3yJ9DlRcGXrfzpFiwvNzG9sx0ZxlGN8W2s2&date=$date')));

    print(list[0].body);

    return list.map((response) {
      // do processing here and return planets data
      return PlanetsModel.fromJson(json.decode(response.body));
    }).toList();
  }
}
