import 'package:flutter/material.dart';

class Constants {
  static const apiKey = 'z7zkM3yJ9DlRcGXrfzpFiwvNzG9sx0ZxlGN8W2s2';
  static const baseUrl = 'https://api.nasa.gov/';
  static const textTheme14 =
      TextStyle(color: Colors.white, fontWeight: FontWeight.w200, fontSize: 14);
  static const textTheme16 =
      TextStyle(fontWeight: FontWeight.normal, fontSize: 16);
  static const textTheme16Bold =
      TextStyle(fontWeight: FontWeight.bold, fontSize: 16);
}
