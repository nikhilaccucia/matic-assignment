class PlanetsModel {
  String date;
  String explanation;
  String url;
  String mediaType;
  String title;
  String hdUrl;

  PlanetsModel(
      {this.date,
      this.explanation,
      this.url,
      this.hdUrl,
      this.mediaType,
      this.title});

  PlanetsModel.fromJson(Map<String, dynamic> json) {
    date = json['date'];
    explanation = json['explanation'];
    url = json['url'];
    mediaType = json['media_type'];
    hdUrl = json['hdurl'];
    title = json['title'];
  }
}
